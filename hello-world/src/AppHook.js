import React, {Component, useState} from "react"

function AppHook (){
    const [answer] = useState("no")
        return(
        <div>
            <h1>Hallo ? {answer}</h1>
        </div>
    )
}

// class AppHook extends Component {
//     constructor() {
//         super()
//         this.state = {
//             answer : "yes"
//         }
//     }

//     render() {
//         return(
//                 <div>
//                     <h1> hallo?  {this.state.answer} </h1>
//                 </div>           
//         )
//     }
// }
export default AppHook