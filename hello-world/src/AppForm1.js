import React, {Component} from "react"
// React Docs about Forms:
// https://reactjs.org/docs/forms.html
class AppForm1 extends Component {
    constructor(){
        super()
        this.state = {
            firstName : "" , 
            lastName : "",
            isFriendly : true,
            gender : "male",
            favColor : "blue"
        }
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(event){
        const {type, name,value, checked} = event.target
        type === "checkbox" ? this.setState( {[name] : checked})  : this.setState ({[name] : value}
        )
    }
    handleSubmit(event){
    }   
    render(){
        return (
            <form onSubmit = {this.handleSubmit}>
                <input 
                    type="text" 
                    value = {this.state.firstName}
                    name = "firstName"
                    placeHolder = "first Name" 
                    onChange = {this.handleChange} >
                </input>
                <br></br>
                <input 
                    type="text" 
                    value =  {this.state.lastName}
                    name = "lastName"
                    placeHolder = "last Name" 
                    onChange = {this.handleChange} >
                </input>                
                <p>{this.state.firstName}</p>
                <p>{this.state.lastName}</p>
               
                <textarea value = {"default value text"}> ff</textarea>
                <br></br>
                <label>is Friedly</label>
                <input 
                    type="checkbox"
                    name = "isFriendly"
                    checked = {this.state.isFriendly}
                    onChange = {this.handleChange}
                ></input>
                <br></br>

                <br></br>
     
                <input 
                    type="radio"
                    name = "gender"
                    value = "male"
                    checked = {this.state.gender === "male"}
                    onChange = {this.handleChange}
                ></input>
                <label>Male</label>
                <br></br>
                <br></br>

                <input 
                    type="radio"
                    name = "gender"
                    value = "female"
                    checked = {this.state.gender === "female"}
                    onChange = {this.handleChange}
                ></input>
                <label>Female</label>
                <br></br>
                <p> you are a {this.state.gender}</p>
                
                <select
                    name = "favColor"
                    value = {this.state.favColor}
                    onChange = {this.handleChange} 
                >
                    <option value = "blue">Blue</option>
                    <option value = "green">Green</option>
                    <option value = "yellow">Yellow</option>
                    <option value = "red">Red</option>
                    <option value = "orange">Orange</option>
                </select>
                <br></br>
                <p> color selected is -  {this.state.favColor}</p>
                <button>Submit</button>
            </form>
                    /**
                     * Other useful form elements:
                     * 
                     *  <textarea /> element
                     *  <input type="checkbox" />
                     *  <input type="radio" />
                     *  <select> and <option> elements
                     */
        )
    }
}
export default AppForm1