import React from "react"

function FormComponent(props) {

    return(
        <main>
            <form>
                <input
                    type = "text"
                    name = "firstName"
                    value = {props.firstName}
                    placeholder = "First Name"
                    onChange = {props.hanldeChange}
                >
                </input>
                <br/>
                <input
                    type = "text"
                    name = "lastName"
                    value = {props.lastName}
                    placeholder = "Last Name"
                    onChange = {props.hanldeChange}
                >
                </input>
                <br/>
                <input
                    type = "text"
                    name = "age"
                    value = {props.age}
                    placeholder = "Age"
                    onChange = {props.hanldeChange}
                >
                </input>
                <br/>
                <br/>
                {/* Create radio buttons for gender here */}
                <label>
                    <input
                        type = "radio"
                        name = "gender"
                        value = "male"
                        checked = {props.gender === "male"}
                        placeholder = "male"
                        onChange = {props.hanldeChange}
                    >
                    </input>
                    <label>Male</label>
                    <br />
                    <input
                        type = "radio"
                        name = "gender"
                        value = "female"
                        checked = {props.gender === "female"}
                        placeholder = "male"
                        onChange = {props.hanldeChange}
                    >
                    </input>
                    <label>Female</label>
                    <br />
                </label>
                <br/>

                {/* Create select box for location here */}
                <br />
                <select 
                    type = "text"
                    name = "destination"
                    value = {props.destination}
                    onChange = {props.hanldeChange}
                    >
                    <option value = "--">Please choose a destination</option>
                    <option value = "Lahore">Lahore</option>
                    <option value = "Islamabad ">Islamabad</option>
                    <option value = "Karachi">Karachi</option>
                    <option value = "Peshawar">Peshawar</option>
                </select>
                
                {/* Create check boxes for dietary restrictions here */}
                <br />
                <br />
                <label>
                    <input 
                        type = "checkbox"
                        name = "isVegan"
                        checked = {props.dietryRestrictions.isVegan}
                        onChange = {props.hanldeChange}
                    />
                    <label> is Vegan</label>
                    <br/>
                    
                    <input 
                        type = "checkbox"
                        name = "isKoscher"
                        checked = {props.dietryRestrictions.isKoscher}
                        onChange = {props.hanldeChange}
                    />
                    <label> is Koscher</label>
                    <br/>
                    <input 
                        type = "checkbox"
                        name = "isLactoseFree"
                        checked = {props.dietryRestrictions.isLactoseFree}
                        onChange = {props.hanldeChange}
                    />  
                    <label> is Lactose Free</label>

                    <br/>

                </label>
                <br/>
                <br/>                    
                <button>Submit</button>
            </form>
            <hr />
            <h2>Entered information:</h2>
            <p>Your name: {props.firstName} {props.lastName}</p>
            <p>Your age: {props.age}</p>
            <p>Your gender: {props.gender}</p>
            <p>Your destination: {props.destination}</p>
            <p> Your dietary restrictions:   </p>
            <p>    Vegan : {props.dietryRestrictions.isVegan ? "Yes" : "No"}  </p>
            <p>    Koscher :  {props.dietryRestrictions.isKoscher ? "Yes" : "No"}  </p>
            <p>    Lactose Free : {props.dietryRestrictions.isLactoseFree ? "Yes" : "No"}    </p>

        </main>
    )
}

export default FormComponent