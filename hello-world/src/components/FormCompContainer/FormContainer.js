import React, {Component} from "react"
import FormComponent from "./FormComponent"


class FormContainers extends Component {
    constructor(){
        super()
        this.state = {
            firstName : "",
            lastName : "",
            age : "",
            gender : "",
            destination : "",
            dietryRestrictions : {
                isVegan : false,
                isKoscher : false,
                isLactoseFree : false
            }
            
        }
        this.hanldeChange = this.hanldeChange.bind(this)
    }
    hanldeChange(event) {
        const {name, value, type, checked} = event.target
        type === "checkbox" ? 
        this.setState ( prevState => { 
            return{
                dietryRestrictions : {
                    /* ...prevState.dietryRestrictions, */
                    [name] : checked
                } 
            }
        }) :
        this.setState ({
            [name] : value
        })
        
    }
  
    
    render(){
        return(
        <FormComponent
            hanldeChange = {this.hanldeChange}
            {...this.state}
        
        />
        )
    }
}
export default FormContainers


/**
 * Other modern/advanced React features/topics to learn:
 * 
 * Official React Context API - https://reactjs.org/docs/context.html
 * Error Boundaries - https://reactjs.org/docs/error-boundaries.html
 * render props - https://reactjs.org/docs/render-props.html
 * Higher Order Components - https://reactjs.org/docs/higher-order-components.html
 * React Router - https://reacttraining.com/react-router/core/guides/philosophy
 * React Hooks - https://reactjs.org/docs/hooks-intro.html
 * React lazy, memo, and Suspense - https://reactjs.org/blog/2018/10/23/react-v-16-6.html
 */