import React, {Component} from "react"
class AppFormPractice extends Component {
    constructor(){
        super()
        this.state = {
            firstName : "",
            lastName : "",
            age : "",
            gender : "",
            destination : "",
            dietryRestrictions : {
                isVegan : false,
                isKoscher : false,
                isLactoseFree : false
            }
            
        }
        this.hanldeChange = this.hanldeChange.bind(this)
    }
    hanldeChange(event) {
        const {name, value, type, checked} = event.target
        type === "checkbox" ? 
        this.setState ( prevState => { 
            return{
                dietryRestrictions : {
                    /* ...prevState.dietryRestrictions, */
                    [name] : checked
                } 
            }
        }) :
        this.setState ({
            [name] : value
        })
        
    }
  
    
    render(){
        return(
            <main>
                <form>
                    <input
                        type = "text"
                        name = "firstName"
                        value = {this.state.firstName}
                        placeholder = "First Name"
                        onChange = {this.hanldeChange}
                    >
                    </input>
                    <br/>
                    <input
                        type = "text"
                        name = "lastName"
                        value = {this.state.lastName}
                        placeholder = "Last Name"
                        onChange = {this.hanldeChange}
                    >
                    </input>
                    <br/>
                    <input
                        type = "text"
                        name = "age"
                        value = {this.state.age}
                        placeholder = "Age"
                        onChange = {this.hanldeChange}
                    >
                    </input>
                    <br/>
                    <br/>
                    {/* Create radio buttons for gender here */}
                    <label>
                        <input
                            type = "radio"
                            name = "gender"
                            value = "male"
                            checked = {this.state.gender === "male"}
                            placeholder = "male"
                            onChange = {this.hanldeChange}
                        >
                        </input>
                        <label>Male</label>
                        <br />
                        <input
                            type = "radio"
                            name = "gender"
                            value = "female"
                            checked = {this.state.gender === "female"}
                            placeholder = "male"
                            onChange = {this.hanldeChange}
                        >
                        </input>
                        <label>Female</label>
                        <br />
                    </label>
                    <br/>

                    {/* Create select box for location here */}
                    <br />
                    <select 
                        type = "text"
                        name = "destination"
                        value = {this.state.destination}
                        onChange = {this.hanldeChange}
                        >
                        <option value = "--">Please choose a destination</option>
                        <option value = "Lahore">Lahore</option>
                        <option value = "Islamabad ">Islamabad</option>
                        <option value = "Karachi">Karachi</option>
                        <option value = "Peshawar">Peshawar</option>
                    </select>
                    
                    {/* Create check boxes for dietary restrictions here */}
                    <br />
                    <br />
                    <label>
                        <input 
                            type = "checkbox"
                            name = "isVegan"
                            checked = {this.state.dietryRestrictions.isVegan}
                            onChange = {this.hanldeChange}
                        />
                        <label> is Vegan</label>
                        <br/>
                        
                        <input 
                            type = "checkbox"
                            name = "isKoscher"
                            checked = {this.state.dietryRestrictions.isKoscher}
                            onChange = {this.hanldeChange}
                        />
                        <label> is Koscher</label>
                        <br/>
                        <input 
                            type = "checkbox"
                            name = "isLactoseFree"
                            checked = {this.state.dietryRestrictions.isLactoseFree}
                            onChange = {this.hanldeChange}
                        />  
                        <label> is Lactose Free</label>

                        <br/>

                    </label>
                    <br/>
                    <br/>                    
                    <button>Submit</button>
                </form>
                <hr />
                <h2>Entered information:</h2>
                <p>Your name: {this.state.firstName} {this.state.lastName}</p>
                <p>Your age: {this.state.age}</p>
                <p>Your gender: {this.state.gender}</p>
                <p>Your destination: {this.state.destination}</p>
                <p> Your dietary restrictions:   </p>
                <p>    Vegan : {this.state.dietryRestrictions.isVegan ? "Yes" : "No"}  </p>
                <p>    Koscher :  {this.state.dietryRestrictions.isKoscher ? "Yes" : "No"}  </p>
                <p>    Lactose Free : {this.state.dietryRestrictions.isLactoseFree ? "Yes" : "No"}    </p>

            </main>
        )
    }
}
export default AppFormPractice