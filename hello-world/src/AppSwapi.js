import React, {Component} from "react"

// https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch
// https://swapi.co/
//https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-promise-27fc71e77261
//https://itnext.io/javascript-promises-vs-rxjs-observables-de5309583ca2

/* The main difference between callbacks and promises is that with callbacks 
you tell the executing function what to do when the asynchronous task completes,
 whereas with promises the executing function returns a special object to you 
 (the promise) and then you tell the promise what to do when the asynchronous 
 task completes.
*/
class AppSwapi extends Component {
    constructor() {
        super()
        this.state = {
            isLoading : false,
            character : {}
        }

    }
    componentDidMount(){
        this.setState( {isLoading : true})
        fetch("https://swapi.co/api/people/1")
            .then(response => response.json())
            .then( data => {
                this.setState ({
                    character : data,
                    isLoading : false
                })
            })   
    }
    render() {
        const text = this.state.isLoading ? "loading...." : this.state.character.name
        return(
            <div> {text} </div>
            
        ) 
    }
}



export default AppSwapi